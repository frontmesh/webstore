import { filterProductsByCategory, mapCategories } from '../index';
import db from '../../db/db.json';
import visProducts from '../../__data__/fish';

describe('helpers', () => {
  describe('filterProductsByCategory', () => {
    it('should return filtered list of products', () => {
      expect(filterProductsByCategory(db.products, 'vis')).toEqual(visProducts);
    });
  });

  describe('mapCategories', () => {
    it('should return unique categories', () => {
      expect(mapCategories(db.products)).toEqual([
        'alles',
        'fruit',
        'vlees',
        'vis'
      ]);
    });
  });
});
