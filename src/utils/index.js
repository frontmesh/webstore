import { compose, map, prop, uniq, prepend } from 'ramda';

export const filterProductsByCategory = (products, category) =>
  products.filter(p => p.category === category);

export const mapCategories = compose(
  prepend('alles'),
  uniq,
  map(prop('category'))
);
