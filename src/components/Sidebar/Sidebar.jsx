import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './sidebar.module.scss';

const Sidebar = ({ categories }) => {
  return (
    <aside className={styles.Sidebar}>
      <nav>
        <ul>
          {categories.map(name => (
            <li key={name} className={styles.NavLink}>
              <NavLink to={`/products/${name}`}>{name}</NavLink>
            </li>
          ))}
        </ul>
      </nav>
    </aside>
  );
};

export default Sidebar;
