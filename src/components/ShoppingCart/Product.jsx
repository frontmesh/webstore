import React from 'react';
import styles from './shoppingCart.module.scss';

const Product = ({
  name,
  count,
  id,
  price,
  addToCartHandler,
  removeFromCartHandler
}) => {
  return (
    <li>
      <div>
        <span>{count}</span>
        <span>&nbsp;x&nbsp;</span>
        <span>{name}</span>
      </div>
      <div className={styles.Price}>
        <button onClick={() => addToCartHandler(id)}>+</button>
        <button onClick={() => removeFromCartHandler(id)}>-</button>
        <span id='price'>&euro;{price}</span>
      </div>
    </li>
  );
};

export default Product;
