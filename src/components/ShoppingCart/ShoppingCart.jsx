import React, { memo } from 'react';

import Product from './Product';
import styles from './shoppingCart.module.scss';

const ShoppingCart = ({
  products,
  total,
  emptyCartHandler,
  addToCartHandler,
  removeFromCartHandler
}) => {
  const isCartEmpty = !(products && products.length);

  const emptyMessage = <p id='msg'>Geen producten toegevoegd.</p>;

  return (
    <aside className={styles.ShoppingCart}>
      <h2>Winkelwagen</h2>
      {isCartEmpty ? (
        emptyMessage
      ) : (
        <ul className={styles.CartList}>
          {products.map(p => (
            <Product
              key={p.name}
              {...p}
              addToCartHandler={addToCartHandler}
              removeFromCartHandler={removeFromCartHandler}
            />
          ))}
        </ul>
      )}
      {!isCartEmpty && (
        <div>
          <p id='total' className={styles.Total}>
            <strong>Total:</strong>
            <strong>&euro;{total}</strong>
          </p>
        </div>
      )}
      {!isCartEmpty && (
        <button id='empty' onClick={() => emptyCartHandler()}>
          Leeg Winkelwagen
        </button>
      )}
    </aside>
  );
};

export default memo(ShoppingCart);
