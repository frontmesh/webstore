import React from 'react';
import { shallow } from 'enzyme';

import ShoppingCart from '../ShoppingCart';

const list = [
  {
    id: 1,
    price: 5,
    name: 'Apple',
    count: 5
  },
  {
    id: 2,
    price: 5,
    name: 'Banana',
    count: 5
  }
];

describe('ShoppingCart component', () => {
  const createComponent = ({ total, products = [] }) => {
    const actions = {
      emptyCartHandler: jest.fn(),
      addToCartHandler: jest.fn(),
      removeFromCartHandler: jest.fn()
    };

    const component = shallow(
      <ShoppingCart products={products} total={total} {...actions} />
    );
    return {
      component,
      actions,
      msg: component.find('#msg'),
      total: component.find('#total'),
      empty: component.find('button#empty'),
      ul: component.find('ul')
    };
  };

  it('should show empty cart message', () => {
    const { msg, total, ul } = createComponent({});
    expect(msg.text()).toEqual('Geen producten toegevoegd.');
    expect(total.exists()).toBeFalsy();
    expect(ul.exists()).toBeFalsy();
  });

  it('should render products', () => {
    const { msg, total, ul } = createComponent({ products: list, total: 29 });

    expect(msg.exists()).toBeFalsy();
    expect(total.text()).toEqual('Total:€29');
    expect(ul.children().length).toEqual(2);
  });

  it('should empty products', () => {
    const { msg, total, ul, empty, actions } = createComponent({
      products: list,
      total: 39
    });

    expect(empty.exists()).toBeTruthy();
    empty.simulate('click');
    expect(actions.emptyCartHandler).toBeCalled();
  });
});
