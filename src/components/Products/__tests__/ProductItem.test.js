import React from 'react';
import { shallow } from 'enzyme';
import ProductItem from '../ProductItem';

describe('ProductItem compon total, products = []ent', () => {
  const createComponent = props => {
    const actions = {
      addToCartHandler: jest.fn()
    };

    const component = shallow(
      <ProductItem {...props} addToCart={actions.addToCartHandler} />
    );

    return {
      component,
      actions,
      add: component.find('button'),
      name: component.find('span'),
      img: component.find('img'),
      price: component.find('strong')
    };
  };
  const params = {
    name: 'Apple',
    price: 1.5,
    id: 1,
    image_url: 'apple.png'
  };
  it('should render product with attributes', () => {
    const { name, img, price } = createComponent(params);
    expect(name.text()).toEqual('Apple');
    expect(img.prop('src')).toEqual('/images/apple.png');
    expect(price.text()).toEqual('€ 1.5');
  });

  it('should add item to cart', () => {
    const { add, actions } = createComponent(params);

    add.simulate('click');
    expect(actions.addToCartHandler).toHaveBeenCalled();
  });
});
