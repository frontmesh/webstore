import React from 'react';
import PropTypes from 'prop-types';

import styles from './products.module.scss';

const ProductItem = ({ id, name, image_url, price, addToCart }) => {
  return (
    <li className={styles.Product}>
      <div className={styles.ProductTop}>
        <span>{name}</span>
        <img src={process.env.PUBLIC_URL + `/images/${image_url}`} alt={name} />
      </div>
      <div className={styles.ProductBottom}>
        <strong className={styles.Price}>&euro; {price}</strong>
        <button className={styles.AddButton} onClick={() => addToCart(id)}>
          Voeg Toe
        </button>
      </div>
    </li>
  );
};

ProductItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  image_url: PropTypes.string.isRequired,
  addToCart: PropTypes.func.isRequired
};

export default ProductItem;
