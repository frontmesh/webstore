import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { ProductListType } from '../../props/';

import { filterProductsByCategory } from '../../utils/';

import ProductItem from './ProductItem';

import styles from './products.module.scss';

const Products = ({ products, filter, addToCart }) => {
  let filtered;
  if (products) {
    filtered =
      filter === 'alles'
        ? products
        : filterProductsByCategory(products, filter);
  }
  return (
    <div>
      <ul className={styles.Products}>
        {filtered &&
          filtered.map(({ id, name, image_url, price }) => (
            <ProductItem
              key={id}
              id={id}
              name={name}
              image_url={image_url}
              price={price}
              addToCart={addToCart}
            />
          ))}
      </ul>
    </div>
  );
};

Products.propTypes = {
  products: ProductListType,
  filter: PropTypes.string.isRequired,
  addToCart: PropTypes.func.isRequired
};

export default memo(Products);
