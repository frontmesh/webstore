import React from 'react';
import logo from '../../logo.svg';
import styles from './layout.module.scss';

export default ({ children }) => {
  return (
    <div className={styles.App}>
      <header className={styles.AppHeader}>
        <img src={logo} className={styles.AppLogo} alt='logo' />
        <p>welkom in onze webshop</p>
      </header>
      <section className={styles.AppContent}>{children}</section>
      <footer />
    </div>
  );
};
