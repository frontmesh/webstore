export default {
  11: {
    id: 11,
    name: 'Garnaal',
    category: 'vis',
    price: 3.1,
    image_url: 'vis/garnaal.jpg'
  },
  12: {
    id: 12,
    name: 'Krab',
    category: 'vis',
    price: 2.5,
    image_url: 'vis/krab.jpg'
  },
  13: {
    id: 13,
    name: 'Vis',
    category: 'vis',
    price: 1.4,
    image_url: 'vis/vis.jpg'
  }
}