import { prop } from 'ramda';

import db from '../db/db.json';

// emulate api call
export const findProducts = () => Promise.resolve(prop('products', db));

export const httpGet = async (f, fetcher) => {
  try {
    const result = await fetcher();
    f(result);
  } catch (err) {
    console.error(`Oops, something went wrong with data fetching`, err);
  }
};
