import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import App from './App';
describe('App container', () => {
  const mockStore = configureStore();
  const initialStore = {
    cart: {
      addedProducts: [],
      countById: {}
    },
    products: {
      byId: {},
      productList: [],
      categoryList: []
    }
  };
  it('renders without crashing', () => {
    const store = mockStore(initialStore);
    const component = shallow(
      <Provider store={store}>
        <App />
      </Provider>
    );
    expect(component.render()).toMatchSnapshot();
  });
});
