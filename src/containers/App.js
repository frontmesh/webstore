import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { matchPath } from 'react-router';
import { connect } from 'react-redux';

import { ProductListType } from '../props/';

import {
  addToCart,
  removeFromCart,
  emptyCart
} from '../store/modules/cart/cartActions';

import { getProducts } from '../store/modules/products/productsActions';
import {
  getInventory,
  getCartInventory,
  getTotal
} from '../store/modules/products/productsSelectors';

import Layout from '../components/Layout/Layout';
import Sidebar from '../components/Sidebar/Sidebar';
import Products from '../components/Products/Products';
import ShoppingCart from '../components/ShoppingCart/ShoppingCart';

const filterMatcher = path =>
  matchPath(path, {
    path: '/products/:id',
    exact: true,
    strict: false
  });

const App = ({
  history,
  addToCart,
  removeFromCart,
  emptyCart,
  cartInventory,
  getProducts,
  inventory,
  total
}) => {
  const matchedPath = history && filterMatcher(history.location.pathname);
  const filter = matchedPath ? matchedPath.params.id : 'alles';

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <Layout>
      <Sidebar categories={inventory.categoryList} />
      <Products
        products={inventory.productList}
        filter={filter}
        addToCart={addToCart}
      />
      <ShoppingCart
        products={cartInventory}
        total={total}
        emptyCartHandler={emptyCart}
        addToCartHandler={addToCart}
        removeFromCartHandler={removeFromCart}
      />
    </Layout>
  );
};

App.propTypes = {
  addToCart: PropTypes.func.isRequired,
  removeFromCart: PropTypes.func.isRequired,
  emptyCart: PropTypes.func.isRequired,
  cartInventory: ProductListType
};

const mapStateToProps = state => ({
  inventory: getInventory(state),
  cartInventory: getCartInventory(state),
  total: getTotal(state)
});

export default connect(
  mapStateToProps,
  { addToCart, removeFromCart, getProducts, emptyCart }
)(memo(App));
