import { getCartInventory, getTotal } from '../productsSelectors';

describe('productSelectors', () => {
  const state = {
    cart: { addedProducts: [11, 2], countById: { 11: 5, 2: 4 } },
    products: {
      byId: {
        11: {
          name: 'first',
          id: 11,
          price: 1.15
        },
        2: {
          name: 'second',
          id: 2,
          price: 2.2
        },
        3: {
          name: 'third',
          id: 3,
          price: 3
        }
      }
    }
  };

  it('getCartInventory should return cart inventory', () => {
    expect(getCartInventory(state)).toEqual([
      {
        id: 11,
        name: 'first',
        count: 5,
        price: 1.15
      },
      {
        id: 2,
        name: 'second',
        count: 4,
        price: 2.2
      }
    ]);
  });

  it('getTotal should return total number', () => {
    expect(getTotal(state)).toEqual('14.55');
  });
});
