import { LOAD_PRODUCTS } from '../productsActions';
import productsReducer, { initialState } from '../productsReducer';

import { default as products } from '../../../../__data__/fish';
import fishById from '../../../../__data__/fishById';


describe('productReducer', () => {
  it('should return initial state', () => {
    expect(productsReducer(undefined, {})).toEqual(initialState);
  });

  it('should return loaded products by id', () => {
    expect(productsReducer(initialState, {
      type: LOAD_PRODUCTS,
      products
    })).toEqual({
      productList: products,
      byId: fishById,
      categoryList: ['alles', 'vis']
    });
  });
});