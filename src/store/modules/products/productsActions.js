import {
  httpGet,
  findProducts
} from '../../../api'
export const LOAD_PRODUCTS = 'LOAD_PRODUCTS';

export const loadProducts = (products) => ({
  type: LOAD_PRODUCTS,
  products
})
export const getProducts = () => (dispatch) => {
  httpGet(products => {
    dispatch(loadProducts(products));
  }, findProducts)
}