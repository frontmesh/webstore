import { getCartAddedProducts, getCountById } from '../cart/cartSelectors';

export const getInventory = state => state.products;
export const getInventoryById = state => state.products.byId;
export const getProductById = (state, id) => getInventoryById(state)[id];
export const getCountPerId = (state, id) => getCountById(state)[id];
export const getCartInventory = state =>
  getCartAddedProducts(state).map(id => {
    return { ...getProductById(state, id), count: getCountPerId(state, id) };
  });

export const getTotal = state =>
  getCartAddedProducts(state)
    .reduce((total, id) => {
      return total + getProductById(state, id).price * getCountPerId(state, id);
    }, 0)
    .toFixed(2);
