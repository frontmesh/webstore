import { combineReducers } from 'redux'
import { indexBy, prop } from 'ramda';
import { LOAD_PRODUCTS } from './productsActions';
import { mapCategories } from '../../../utils/';

export const initialState = {
  byId: {},
  productList: [],
  categoryList:[],
};
const byId = (state = {}, action) => {
  switch (action.type) {
    case LOAD_PRODUCTS: {
      return {
        ...state,
        ...indexBy(prop('id'), action.products)
      }
    }
    default:
      return state;
  }
};

const productList = (state=[], action) => {
  switch (action.type) {
    case LOAD_PRODUCTS: {
      return [
        ...state,
        ...action.products
      ]
    }
    default:
      return state;
  }
}

const categoryList = (state=[], action) => {
  switch (action.type) {
    case LOAD_PRODUCTS: {
      return [
        ...state,
        ...mapCategories(action.products)
      ]
    }
    default:
      return state;
  }
}

export default combineReducers({ byId, productList, categoryList });
