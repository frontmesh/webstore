import { combineReducers } from 'redux';

import { default as cart } from './cart/cartReducer';
import { default as products } from './products/productsReducer';

export default combineReducers({
  cart,
  products
});
