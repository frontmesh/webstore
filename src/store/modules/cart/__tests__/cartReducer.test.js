import cartReducer from '../cartReducer';

describe('cart reducer', () => {
  const initialState = {
    addedProducts: [],
    countById: {}
  };

  it('should provide initial state', () => {
    expect(cartReducer(undefined, {})).toEqual(initialState);
  });

  it('should add an item to cart', () => {
    expect(cartReducer(initialState, { type: 'ADD_TO_CART', id: 3 })).toEqual({
      addedProducts: [3],
      countById: { 3: 1 }
    });
  });

  it('should add an additional item to cart', () => {
    expect(
      cartReducer(
        {
          addedProducts: [3],
          countById: { 3: 1 }
        },
        { type: 'ADD_TO_CART', id: 3 }
      )
    ).toEqual({
      addedProducts: [3],
      countById: { 3: 2 }
    });
  });

  it('should remove an item from a cart', () => {
    expect(
      cartReducer(
        {
          addedProducts: [10, 5],
          countById: { 10: 5, 5: 2 }
        },
        { type: 'REMOVE_FROM_CART', id: 10 }
      )
    ).toEqual({
      addedProducts: [10, 5],
      countById: { 10: 4, 5: 2 }
    });

    expect(
      cartReducer(
        {
          addedProducts: [10, 5],
          countById: { 10: 5, 5: 2 }
        },
        { type: 'REMOVE_FROM_CART', id: 12 }
      )
    ).toEqual({
      addedProducts: [10, 5],
      countById: { 10: 5, 5: 2 }
    });
  });

  it('should remove last item from cart', () => {
    expect(
      cartReducer(
        {
          addedProducts: [10],
          countById: { 10: 1 }
        },
        { type: 'REMOVE_FROM_CART', id: 10 }
      )
    ).toEqual(initialState);
  });

  it('should empty cart', () => {
    expect(
      cartReducer(
        {
          addedProducts: [10, 5],
          countById: { 10: 5, 5: 2 }
        },
        { type: 'EMPTY_CART', id: 12 }
      )
    ).toEqual(initialState);
  });
});
