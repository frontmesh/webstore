export const getCart = state => state.cart;

export const getCartAddedProducts = state => state.cart.addedProducts;
export const getCountById = state => state.cart.countById;
