import * as R from 'ramda';
import { ADD_TO_CART, REMOVE_FROM_CART, EMPTY_CART } from './cartActions';

export const initialState = {
  addedProducts: [],
  countById: {}
};

const cartReducer = (state = initialState, action) => {
  const { addedProducts, countById } = state;

  switch (action.type) {
    case ADD_TO_CART: {
      const { id } = action;
      return {
        ...state,
        addedProducts: R.ifElse(R.contains(id), R.flatten, R.append(id))(
          addedProducts
        ),
        countById: {
          ...countById,
          [id]: (countById[id] || 0) + 1
        }
      };
    }
    case REMOVE_FROM_CART: {
      const { id } = action;
      const idx = addedProducts.indexOf(action.id);
      if (idx === -1) {
        return state;
      }
      const isLast = !(countById[id] > 1);
      return {
        ...state,
        addedProducts: R.ifElse(() => isLast, R.without([id]), R.flatten)(
          addedProducts
        ),
        countById: R.ifElse(
          () => isLast,
          R.omit([id]),
          R.assoc(id, countById[id] - 1)
        )(countById)
      };
    }
    case EMPTY_CART: {
      return initialState;
    }
    default:
      return state;
  }
};

export default cartReducer;
