import App from '../containers/App';

export default [
  {
    path: '/',
    component: App,
    routes: [
      {
        path: '/products/:id',
        component: App
      }
    ]
  }
];
